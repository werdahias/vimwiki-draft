# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/vimwiki/issues
# Bug-Submit: https://github.com/<user>/vimwiki/issues/new
# Changelog: https://github.com/<user>/vimwiki/blob/master/CHANGES
# Documentation: https://github.com/<user>/vimwiki/wiki
# Repository-Browse: https://github.com/<user>/vimwiki
# Repository: https://github.com/<user>/vimwiki.git
